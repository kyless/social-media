/* eslint-disable @typescript-eslint/explicit-function-return-type */
import "../styles.css";

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}

export default MyApp;
