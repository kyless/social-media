import TextField from "../components/TextField";
import Button from "../components/Button";

export default function Signup(): JSX.Element {
  return (
    <div className="container">
      <h1>Sign Up</h1>
      <TextField type="text" placeholder="Email" />
      <TextField type="password" placeholder="Password" />
      <TextField type="password" placeholder="Retry Password" />
      <Button text="Submit" />
      <style jsx>{`
        .container {
          height: 100vh;
          justify-content: center;
          align-items: center;
          display: flex;
          flex: 1;
          flex-direction: column;
        }
      `}</style>
    </div>
  );
}
