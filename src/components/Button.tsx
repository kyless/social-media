export interface ButtonProps {
  text: string;
}

export default function TextField(props: ButtonProps): JSX.Element {
  return (
    <div>
      <button>{props.text}</button>
      <style jsx>{`
        button {
          margin-bottom: 10px;
          flex-grow: 1;
        }
      `}</style>
    </div>
  );
}
