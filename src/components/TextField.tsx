export interface TextFieldProps {
  type: string;
  placeholder: string;
}

export default function TextField(props: TextFieldProps): JSX.Element {
  return (
    <div>
      <input
        className="input"
        type={props.type}
        placeholder={props.placeholder}
      />
      <style jsx>{`
        .input {
          margin-bottom: 10px;
          flex-grow: 1;
        }
      `}</style>
    </div>
  );
}
